import { apiError } from '../_actions/api';
import { DEFAULT_FAIL_SUFFIX } from '../_actions/types';
const errorHandler = store => next => action => {
  if (action.error && action.type.endsWith(DEFAULT_FAIL_SUFFIX)) {
    store.dispatch(apiError(action.payload));
    return Promise.reject(action.payload);
  }
  return next(action);
};

export default errorHandler;
