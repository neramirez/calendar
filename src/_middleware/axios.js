import AXIOS_CLIENT from './client';

import {
  RAM,
  DEFAULT_FAIL_SUFFIX,
  DEFAULT_SUCCESS_SUFFIX
} from '../_actions/types';
import { apiError, apiStart, apiEnd } from '../_actions/api';

/**
 * Is the argument a plain object?
 * Inspired by lodash.isplainobject
 *
 * @function isPlainObject
 * @param {object} obj - The object to check
 * @returns {boolean}
 */

function isPlainObject(obj) {
  return (
    obj &&
    typeof obj == 'object' &&
    Object.getPrototypeOf(obj) === Object.prototype
  );
}
/**
 * Is the given action a plain JavaScript object with an [RAM] property?
 *
 * @function isRAM
 * @access public
 * @param {object} action - The action to check
 * @returns {boolean}
 */

function isRAM(action) {
  return isPlainObject(action) && action[RAM];
}

function getDataOrParams(method) {
  return ['GET', 'DELETE'].includes(method) ? 'params' : 'data';
}

const apiMiddleware = store => next => action => {
  const transformRequestIfFileUpload = fileUpload => {
    return (data, headers) => {
      if (fileUpload) {
        headers['Content-Type'] = 'multipart/form-data';
        const formData = new FormData();
        Object.keys(data).forEach(item => {
          if (data[item]) {
            if (data[item].file) {
              formData.append(item, data[item].file);
            } else {
              formData.append(item, data[item]);
            }
          }
        });
        return formData;
      } else {
        headers['Content-Type'] = 'application/json';
        return JSON.stringify(data);
      }
    };
  };

  if (isRAM(action)) {
    const { dispatch } = store;
    const { type, endpoint, method, body, fileUpload } = action[RAM];

    const actionWith = data => {
      const finalAction = Object.assign({}, action, data);
      delete finalAction[RAM];
      return finalAction;
    };

    const sendRequest = (endpoint, method, data, fileUpload) => {
      const dataOrParams = getDataOrParams(method);
      return AXIOS_CLIENT.request({
        url: `${endpoint}&appId=${process.env.REACT_APP_API_KEY}`,
        method,
        [dataOrParams]: data,
        transformRequest: transformRequestIfFileUpload(fileUpload)
      });
    };
    const handleSuccess = response => {
      return Promise.resolve(
        next({
          type: `${type}${DEFAULT_SUCCESS_SUFFIX}`,
          payload: response.data
        })
      );
    };

    const handlerError = error => {
      if (error.isAxiosError) {
        const axiosError = error.toJSON();

        if (!error.response) {
          dispatch(apiError(axiosError));
        }

        next({
          type: `${type}${DEFAULT_FAIL_SUFFIX}`,
          error: axiosError
        });
        return Promise.reject(axiosError);
      } else {
        dispatch(apiError(error.response.data));
        next({
          type: `${type}${DEFAULT_FAIL_SUFFIX}`,
          error: error.response.data
        });
        return Promise.reject(error);
      }
    };
    const finish = () => dispatch(apiEnd(action[RAM]));
    dispatch(apiStart(action[RAM]));

    next(actionWith({ type: type }));

    return sendRequest(endpoint, method, body, fileUpload)
      .then(handleSuccess)
      .catch(handlerError)
      .finally(finish);
  } else {
    return next(action);
  }
};

export default apiMiddleware;
