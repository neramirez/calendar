export const RAM = '@@redux-axios-middleware/RMA';

export const DEFAULT_SUCCESS_SUFFIX = '_SUCCESS';
export const DEFAULT_FAIL_SUFFIX = '_FAIL';
export const API_START = 'API_START';
export const API_END = 'API_END';
export const SHOW_ERROR_ACTION = 'SHOW_ERROR_ACTION';
export const ADD_ERRORS = 'ADD_ERRORS';
export const GET_ERRORS = 'GET_ERRORS';
export const ADD_REMINDER = 'ADD_REMINDER';
export const SELECT_REMINDER = 'SELECT_REMINDER';
export const DELETE_REMINDER = 'DELETE_REMINDER';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const GET_FORECAST = 'GET_FORECAST';
export const DELETE_ALL_REMINDERS = 'DELETE_ALL_REMINDERS';
