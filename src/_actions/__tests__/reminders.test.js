import configureStore from 'redux-mock-store';
import { addReminder } from '../reminders.actions';
import { ADD_REMINDER } from '../types';
import {
  addReminderData,
  reminderReducerData
} from '../__fixtures__/ReminderData';

describe('add reminder redux', () => {
  const mockStore = configureStore();
  const reduxStore = mockStore();

  beforeEach(() => {
    reduxStore.clearActions();
  });

  describe('add reminder action', () => {
    it('should dispatch the add reminder action', () => {
      const expectedActions = [
        {
          payload: addReminderData,
          type: ADD_REMINDER
        }
      ];
      reduxStore.dispatch(addReminder(reminderReducerData));

      expect(reduxStore.getActions()).toEqual(expectedActions);
    });
  });
});
