import { ADD_REMINDER, DELETE_ALL_REMINDERS, DELETE_REMINDER, SELECT_REMINDER } from './types';

export const addReminder = reminder => ({
  type: ADD_REMINDER,
  payload: reminder
});

export const selectReminder = reminderId => ({
  type: SELECT_REMINDER,
  payload: reminderId
});


export const deleteReminder = reminderId => ({
  type: DELETE_REMINDER,
  payload: reminderId
});


export const deleteAllRemindersForDate = date => ({
  type: DELETE_ALL_REMINDERS,
  payload: date
});
