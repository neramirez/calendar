import { API_START, API_END, SHOW_ERROR_ACTION } from '../_actions/types';

/**
 *
 * @param label
 * @returns {{payload: *, type: *}}
 */
export const apiStart = label => ({
  type: API_START,
  payload: label
});

export const apiEnd = label => ({
  type: API_END,
  payload: label
});

export const apiError = error => ({
  type: SHOW_ERROR_ACTION,
  payload: error
});
