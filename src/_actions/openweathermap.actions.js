import { GET_FORECAST, RAM } from './types';

export const getForecastByCityName = cityName => {
  return {
    [RAM]: {
      endpoint: `forecast?q=${cityName}`,
      method: 'GET',
      type: GET_FORECAST
    }
  };
};
