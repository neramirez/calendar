import React from 'react';
import { CirclePicker } from 'react-color';

export const renderField = ({
  input,
  type,
  name,
  placeholder,
  meta: { touched, error, warning }
}) => (
  <fieldset>
    <input
      className="form-control"
      name={name}
      {...input}
      type={type}
      placeholder={placeholder}
    />
    {touched &&
      ((error && <span className="error-control">{error}</span>) ||
        (warning && <span>{warning}</span>))}
  </fieldset>
);
/**
 *
 * @param value
 * @param onChange
 * @returns {*}
 */
export const renderColorPicker = ({ input: { value, onChange } }) => (
  <CirclePicker
    color={value}
    style={{ marginBottom: '10px' }}
    onChange={value => {
      onChange(value.hex);
    }}
  />
);
