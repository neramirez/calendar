import moment from 'moment';

export const getCalendarDates = date => {
  const firstDay = moment(date).startOf('month');
  const dates = [];
  const firstMonthsWeekDay = firstDay.weekday() + 1;
  for (let i = firstMonthsWeekDay * -1 + 1; i <= 0; i++) {
    dates.push(
      moment(firstDay)
        .startOf('month')
        .add(i, 'days')
    );
  }

  for (let i = 1; dates.length < 7 * 6; i++) {
    dates.push(
      moment(firstDay)
        .startOf('month')
        .add(i, 'days')
    );
  }
  return dates;
};
