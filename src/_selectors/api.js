import { createSelector } from 'reselect';

export const getLoadingStatus = createSelector(
  state => state.api.loading,
  loading => loading
);
