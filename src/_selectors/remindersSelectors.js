import { createSelector } from 'reselect';
import { sortBy } from 'lodash';
import moment from 'moment';
export const DATE_FORMAT = 'YYYY-MM-DD';
const getRemindersObject = createSelector(
  state => state.reminders,
  reminders => reminders.items || {}
);

const getSelectedReminderId = createSelector(
  state => state.selectedReminder.selected,
  selectedReminder => selectedReminder.id || null
);
const getRemindersArray = createSelector(
  getRemindersObject,
  reminders => Object.values(reminders)
);
export const getRemindersByDateSortedByTime = date =>
  createSelector(
    getRemindersArray,
    reminders =>
      sortBy(reminders.filter(reminder => reminder.date === date), reminder =>
        moment(reminder.date_time)
      )
  );

export const getSelectedReminder = createSelector(
  getSelectedReminderId,
  getRemindersObject,
  (reminderId, reminders) => {
    if (reminderId) {
      return reminders[reminderId];
    }
  }
);
