import { createSelector } from 'reselect';

const getModalState = state => state.modal;

export const getModalIsOpen = createSelector(
  getModalState,
  modal => modal.open
);
