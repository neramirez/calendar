import { createSelector } from 'reselect';

export const getWeatherIconByCityName = cityName =>
  createSelector(
    state => state.openWeatherMap.byCityName,
    iconsByCityName =>
      iconsByCityName[cityName] ? iconsByCityName[cityName].icon : undefined
  );
