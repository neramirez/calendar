import React from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { ReminderSlot } from '../ReminderSlot/ReminderSlot';
import styles from './DaySlot.module.scss';
import {
  DATE_FORMAT,
  getRemindersByDateSortedByTime
} from '../../_selectors/remindersSelectors';
import { selectReminder } from '../../_actions/reminders.actions';
import { openModal } from '../../_actions/modal.actions';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 * Component that returns a rendered DaySlot, which is each square on the calendar
 * It receives:
 * @param date: a Moment js date
 * @param firstDayOfCurrentMonth: the current month
 * @param reminders: A list of reminders
 * @param selectReminder: A function to select a reminder and add it to the store
 * @param openModal: A function to open the modal
 * @returns A rendered DaySlot
 */
const DaySlot = ({
  date,
  firstDayOfCurrentMonth,
  reminders,
  selectReminder,
  openModal
}) => {
  const classes = classNames({
    [styles.weekend]: date.day() === 0 || date.day() === 6,
    [styles.outside_month]: firstDayOfCurrentMonth.month() !== date.month()
  });
  return (
    <li className={classes}>
      <div className="date">{date.date()}</div>
      <div className={styles.dateReminders}>
        {reminders.map(reminder => (
          <ReminderSlot
            selectReminder={selectReminder}
            key={reminder.id}
            reminder={reminder}
            openModal={openModal}
          />
        ))}
      </div>
    </li>
  );
};

const mapStateToProps = (state, { date }) => {
  const formattedDate = date.format(DATE_FORMAT);
  return {
    reminders: getRemindersByDateSortedByTime(formattedDate)(state)
  };
};
const mapDispatchToProps = dispatch => {
  return {
    selectReminder: reminderId => dispatch(selectReminder(reminderId)),
    openModal: () => dispatch(openModal())
  };
};

DaySlot.propTypes = {
  firstDayOfCurrentMonth: PropTypes.object.isRequired,
  openModal: PropTypes.func.isRequired,
  date: PropTypes.object.isRequired,
  reminders: PropTypes.array,
  selectReminder: PropTypes.func.isRequired
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DaySlot);
