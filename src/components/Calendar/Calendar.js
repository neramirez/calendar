import React from 'react';
import css from './Calendar.module.scss';
import DaySlot from '../DaySlot/DaySlot';
import ReminderModal from '../ReminderModal';
import { openModal } from '../../_actions/modal.actions';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getCalendarDates } from '../../_helpers/calendar';
import moment from 'moment';

/**
 * Functional component that renders a Calendar, receives the following:
 * @param openModal Function to open the edit/create reminders
 * @returns {*}
 */
const Calendar = ({ openModal }) => {
  /**
   *   param currentMonth The current month being displayed on the calendar
   */
  const [firstDayOfMonth, setFirstDayOfMonth] = React.useState(moment());
  /**
   * @param dates An array of dates to display on the calendar, please note that
   * this array contains dates outside currentMonth
   */
  const [calendarDates, setCalendarDates] = React.useState(
    getCalendarDates(firstDayOfMonth)
  );

  const moveTo = units => {
    setFirstDayOfMonth(firstDayOfMonth.add(units, 'month').startOf('month'));

    setCalendarDates(getCalendarDates(firstDayOfMonth));
  };
  return (
    <>
      <ReminderModal />
      <div>
        <div className={css.headerSection}>
          <button onClick={() => moveTo(-1)}>prev</button>
          <button onClick={() => openModal()}>new reminder</button>
          <button onClick={() => moveTo(1)}>next</button>
        </div>

        <ul className={`${css.calendar_header} ${css.month_header}`}>
          <li>
            <abbr title={firstDayOfMonth.format('MM YY')}>
              {firstDayOfMonth.format('MMMM YYYY')}
            </abbr>
           </li>
        </ul>
        <ul className={css.calendar_header}>
          <li>
            <abbr title="S">Sunday</abbr>
          </li>
          <li>
            <abbr title="M">Monday</abbr>
          </li>
          <li>
            <abbr title="T">Tuesday</abbr>
          </li>
          <li>
            <abbr title="W">Wednesday</abbr>
          </li>
          <li>
            <abbr title="T">Thursday</abbr>
          </li>
          <li>
            <abbr title="F">Friday</abbr>
          </li>
          <li>
            <abbr title="S">Saturday</abbr>
          </li>
        </ul>

        <ul className={css.day_grid}>
          {calendarDates.map(date => (
            <DaySlot
              key={date}
              firstDayOfCurrentMonth={firstDayOfMonth}
              date={date}
            />
          ))}
        </ul>
      </div>
    </>
  );
};

Calendar.propTypes = {
  openModal: PropTypes.func.isRequired
};
const mapStateToProps = () => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    openModal: () => dispatch(openModal())
  };
};
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Calendar);
