import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import ReminderForm from '../ReminderForm';

const mockStore = configureStore();
const mockDispatchFn = jest.fn();

describe('<ReminderForm /> Editing a reminder', () => {
  let wrapper;

  const props = {
    handleSubmit: jest.fn()
  };

  const fakeEditState = {
    reminders: {
      items: {
        FAKE_ID: {
          id: '264753e6-4f5f-4f8e-ad50-a219f2e8c1f4',
          title: 'Manual reminder',
          city: 'Bogota',
          date_time: '2019-11-06T01:00',
          color: '#e91e63',
          date: '2019-11-06'
        }
      }
    },
    selectedReminder: { selected: { id: 'FAKE_ID' } },
    api: { loading: false }
  };

  it('defines the component', () => {
    wrapper = mount(
      <Provider store={mockStore(fakeEditState)}>
        <ReminderForm {...props} dispatch={mockDispatchFn} />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });

  it('renders form component on edit mode', () => {
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('input[name="title"]')).toHaveLength(1);
    expect(wrapper.find('input[name="date_time"]')).toHaveLength(1);
    expect(wrapper.find('input[name="city"]')).toHaveLength(1);
    expect(wrapper.find('button[name="submit"]')).toHaveLength(1);
    expect(wrapper.find('button[name="delete"]')).toHaveLength(1);
    expect(wrapper.find('h4').text()).toBe(
      'Fill the form below to edit your reminder'
    );
    expect(wrapper.find('h3').text()).toBe('Edit reminder');
  });
});
