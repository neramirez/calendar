import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import ReminderForm from '../ReminderForm';
import { reducer as formReducer } from 'redux-form';
import { combineReducers, createStore } from 'redux';
import reminders from '../../../_reducers/reminders.reducer';
import selectedReminder from '../../../_reducers/selectedReminder.reducer';
import openWeatherMap from '../../../_reducers/openweathermap.reducer';
import api from '../../../_reducers/api.reducer';

describe('<ReminderForm /> Editing a reminder', () => {
  let wrapper;
  const rootReducer = combineReducers({
    reminders,
    selectedReminder,
    openWeatherMap,
    api,
    form: formReducer
  });

  beforeEach(() => {
    wrapper = mount(
      <Provider store={createStore(rootReducer)}>
        <ReminderForm />
      </Provider>
    );
  });

  describe('input title', () => {
    let titleField;

    beforeEach(() => {
      titleField = wrapper.find('input[name="title"]');
    });

    it('shows error when title is not set', () => {
      titleField.simulate('blur');
      const errorControl = wrapper.find('.error-control');
      expect(errorControl).toHaveLength(1);
      expect(errorControl.text()).toBe('Required');
    });

    it('shows error when title exceeds 30 chars', () => {
      titleField.simulate('change', {
        target: { value: '1234567890123456789012345678901' }
      });
      titleField.simulate('blur');

      const errorControl = wrapper.find('.error-control');

      expect(errorControl).toHaveLength(1);
      expect(errorControl.text()).toBe('Must be 30 characters or less');
    });
  });

  describe('input city', () => {
    let cityField;

    beforeEach(() => {
      cityField = wrapper.find('input[name="city"]');
    });

    it('shows error when city is not set', () => {
      cityField.simulate('blur');
      const errorBlock = wrapper.find('.error-control');
      expect(errorBlock).toHaveLength(1);
      expect(errorBlock.text()).toBe('Required');
    });
  });
});
