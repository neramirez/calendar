import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import ReminderForm from '../ReminderForm';
import { reducer as formReducer } from 'redux-form';
import { combineReducers, createStore } from 'redux';
import reminders from '../../../_reducers/reminders.reducer';
import selectedReminder from '../../../_reducers/selectedReminder.reducer';
import openWeatherMap from '../../../_reducers/openweathermap.reducer';
import api from '../../../_reducers/api.reducer';

describe('<ReminderForm /> Editing a reminder', () => {
  let wrapper, handleSubmit, getWeatherForecast;

  const rootReducer = combineReducers({
    reminders,
    selectedReminder,
    openWeatherMap,
    api,
    form: formReducer
  });

  beforeEach(() => {
    handleSubmit = jest.fn();
    getWeatherForecast = jest.fn();
    wrapper = mount(
      <Provider store={createStore(rootReducer)}>
        <ReminderForm
          getWeatherForecast={getWeatherForecast}
          handleSubmit={handleSubmit}
        />
      </Provider>
    );
  });

  describe('form submit', () => {
    let form;

    beforeEach(() => {
      form = wrapper.find('form');
    });

    it('submit actions', () => {
      const titleField = wrapper.find('input[name="title"]');
      titleField.simulate('change', {
        target: { value: 'FAKE REMINDER' }
      });
      titleField.simulate('blur');

      const dateTimeField = wrapper.find('input[name="date_time"]');
      dateTimeField.simulate('change', {
        target: { value: '2019-11-06T01:00' }
      });
      dateTimeField.simulate('blur');

      form.simulate('submit');

      expect(handleSubmit).toBeCalledTimes(1);
    });
  });
});
