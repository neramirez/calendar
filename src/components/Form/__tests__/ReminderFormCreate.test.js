import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import ReminderForm from '../ReminderForm';

const mockStore = configureStore();
const mockDispatchFn = jest.fn();

describe('<ReminderForm /> Create a reminder', () => {
  let wrapper;

  const props = {
    handleSubmit: jest.fn()
  };

  const fakeEditState = {
    reminders: {
      items: {}
    },
    selectedReminder: { selected: {} },
    api: { loading: false }
  };

  it('defines the component', () => {
    wrapper = mount(
      <Provider store={mockStore(fakeEditState)}>
        <ReminderForm {...props} dispatch={mockDispatchFn} />
      </Provider>
    );
    expect(wrapper).toBeDefined();
  });

  it('renders form component on edit mode', () => {
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find('input[name="title"]')).toHaveLength(1);
    expect(wrapper.find('input[name="date_time"]')).toHaveLength(1);
    expect(wrapper.find('input[name="city"]')).toHaveLength(1);
    expect(wrapper.find('button[name="submit"]')).toHaveLength(1);
    expect(wrapper.find('button[name="delete"]')).toHaveLength(0);
    expect(wrapper.find('h4').text()).toBe(
      'Fill the form below to create a reminder'
    );
    expect(wrapper.find('h3').text()).toBe('Create a new reminder');
  });
});
