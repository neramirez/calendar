import React from 'react';
import css from './ReminderForm.module.scss';
import { Field, formValueSelector, initialize, reduxForm } from 'redux-form';
import { renderColorPicker, renderField } from '../../_helpers/forms';
import { maxLength30, required } from '../../_helpers/validators';
import { WeatherInfo } from '../WeatherInfo/WeatherInfo';
import { getSelectedReminder } from '../../_selectors/remindersSelectors';
import { getWeatherIconByCityName } from '../../_selectors/openWeatherMapSelectors';
import { getLoadingStatus } from '../../_selectors/api';
import {
  addReminder,
  deleteAllRemindersForDate,
  deleteReminder
} from '../../_actions/reminders.actions';
import { closeModal } from '../../_actions/modal.actions';
import { getForecastByCityName } from '../../_actions/openweathermap.actions';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
export const formName = 'ReminderForm';
/**
 * Returns a rendered redux-form connected c
 * @param handleSubmit: The redux form handle submit function
 * @param isEditing: True is the user is editing a reminder, false otherwise
 * @param getWeatherForecast: A Function that dispatches the a request to get
 * the forecasted weather for the entered city
 * @param isLoading: Returns true is an API_START is on the way
 * @param weatherIcon: A weather icon returned by open weather map
 * @param deleteReminder: A function to delete reminders
 * @param reminderId: If is editing the reminder id, otherwise undefined
 * @param cityName: The city name the user entered
 * @returns A rendered redux form with fields: Title, City, Date-Time and Color
 */
const ReminderForm = ({
  handleSubmit,
  isEditing,
  getWeatherForecast,
  isLoading,
  weatherIcon,
  deleteReminder,
  reminderId,
  cityName,
  date,
  deleteAllRemindersForDate
}) => (
  <form className={css.reminderForm} onSubmit={handleSubmit}>
    {!isEditing && <h3>Create a new reminder</h3>}
    {isEditing && <h3>Edit reminder</h3>}
    {isEditing && <h4>Fill the form below to edit your reminder</h4>}
    {!isEditing && <h4>Fill the form below to create a reminder</h4>}
    <Field
      type="text"
      label="Title"
      placeholder="Title"
      component={renderField}
      name="title"
      validate={[required, maxLength30]}
    />
    <Field
      type="text"
      label="City"
      placeholder="City"
      component={renderField}
      name="city"
      validate={[required]}
      onBlur={(event, value) => {
        if (value.length >= 3) {
          getWeatherForecast(value);
        }
      }}
    />

    <WeatherInfo
      isLoading={isLoading}
      weatherIcon={weatherIcon}
      cityName={cityName}
    />

    <Field
      type="datetime-local"
      label="Date"
      placeholder="Date and Time"
      component={renderField}
      name="date_time"
      validate={[required]}
    />
    <Field component={renderColorPicker} name="color" />
    <fieldset>
      <button name="submit" type="submit">
        Save Reminder
      </button>
    </fieldset>
    {isEditing && (
      <>
        <fieldset>
          <button
            name="delete"
            className={css.deleteBtn}
            onClick={() => deleteReminder(reminderId)}
          >
            Delete Reminder
          </button>
        </fieldset>
        <fieldset>
          <button
            name="delete_all"
            className={css.deleteBtn}
            onClick={() => deleteAllRemindersForDate(date)}
          >
            Delete all reminders for this date
          </button>
        </fieldset>
      </>
    )}
  </form>
);

const mapStateToProps = state => {
  const selectedReminder = getSelectedReminder(state);
  const cityName = formValueSelector(formName)(state, 'city');
  return {
    initialValues: selectedReminder,
    isEditing: !!selectedReminder,
    reminderId: selectedReminder ? selectedReminder.id : undefined,
    weatherIcon: cityName
      ? getWeatherIconByCityName(cityName)(state)
      : undefined,
    isLoading: getLoadingStatus(state),
    cityName: cityName,
    date: selectedReminder ? selectedReminder.date : undefined
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: reminderData => {
      dispatch(addReminder(reminderData));
      dispatch(closeModal());
      dispatch(initialize(formName));
    },
    deleteReminder: reminderId => {
      dispatch(deleteReminder(reminderId));
      dispatch(closeModal());
      dispatch(initialize(formName));
    },
    deleteAllRemindersForDate: date => {
      dispatch(deleteAllRemindersForDate(date));
      dispatch(closeModal());
      dispatch(initialize(formName));
    },
    getWeatherForecast: cityName => {
      dispatch(getForecastByCityName(cityName));
    }
  };
};
ReminderForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isEditing: PropTypes.bool.isRequired,
  getWeatherForecast: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  weatherIcon: PropTypes.string,
  deleteReminder: PropTypes.func.isRequired,
  reminderId: PropTypes.string,
  cityName: PropTypes.string,
  date: PropTypes.object,
  deleteAllRemindersForDate: PropTypes.func
};
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    form: formName
  })
)(ReminderForm);
