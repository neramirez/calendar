import React from 'react';
import css from './WeatherInfo.module.scss';
import { PulseLoader } from 'react-spinners';
import PropTypes from 'prop-types';

/**
 *
 * Returns a weather widget for an entered city
 * @param isLoading: Returns true is an ongoing request is happening to open weather map
 * @param weatherIcon: The weather icon returned by open weather map
 * @param cityName: The entered city name
 * @returns A rendered weather widget for an entered city
 */
export const WeatherInfo = ({ isLoading, weatherIcon, cityName }) => (
  <div className={css.weather}>
    {isLoading && <PulseLoader />}
    {!isLoading && weatherIcon && (
      <>
        <div>Weather for your city</div>
        <div>
          <img
            src={`https://openweathermap.org/img/w/${weatherIcon}.png`}
            alt={`Weather forecast for ${cityName}`}
          />
        </div>
      </>
    )}
  </div>
);
WeatherInfo.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  weatherIcon: PropTypes.string,
  cityName: PropTypes.string
};
