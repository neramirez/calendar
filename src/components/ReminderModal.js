import React from 'react';
import Modal from 'react-modal';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import css from './Form/ReminderForm.module.scss';
import { getModalIsOpen } from '../_selectors/modalSelectors';
import { closeModal } from '../_actions/modal.actions';

import ReminderForm, { formName } from './Form/ReminderForm';
import PropTypes from 'prop-types';

/**
 *Returns a react-modal that have a ReminderForm
 * @param closeModal: A function to close modal
 * @param isModalOpen: Tru is this modal is open otherwise false
 * @returns  A rendered ReminderModal
 * @constructor
 */
const ReminderModal = ({ closeModal, isModalOpen }) => {
  return (
    <Modal
      ariaHideApp={false}
      isOpen={isModalOpen}
      onRequestClose={closeModal}
      className={css.modalContent}
      overlayClassName={css.modalOverlay}
    >
      <ReminderForm />
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    isModalOpen: getModalIsOpen(state)
  };
};
const mapDispatchToProps = dispatch => {
  return {
    closeModal: () => {
      dispatch(closeModal());
      dispatch(initialize(formName));
    }
  };
};

ReminderModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ReminderModal);
