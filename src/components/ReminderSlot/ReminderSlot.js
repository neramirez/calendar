import React from 'react';
import css from './ReminderSlote.module.scss';
import PropTypes from 'prop-types';

/**
 * Returns a rendered ReminderSlot
 * @param reminder: the reminder to be rendered
 * @param openModal: A function to open modal on edit mode
 * @param selectReminder: The function to select the reminder and pass it trough
 * the state to the redux form
 * @returns a rendered ReminderSlot
 */
export const ReminderSlot = ({ reminder, openModal, selectReminder }) => {
  return (
    <button
      onClick={() => {
        selectReminder(reminder.id);
        openModal();
      }}
      className={css.reminderSlot}
      style={{ backgroundColor: reminder.color }}
    >
      {reminder.title}
    </button>
  );
};

ReminderSlot.propTypes = {
  reminder: PropTypes.object.isRequired,
  openModal: PropTypes.func.isRequired,
  selectReminder: PropTypes.func.isRequired
};
