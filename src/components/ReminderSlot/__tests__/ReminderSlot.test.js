import React from 'react';
import { shallow } from 'enzyme';
import { ReminderSlot } from '../ReminderSlot';

describe('ReminderSlot component', () => {
  it('starts with a count of 0', () => {
    const wrapper = shallow(
      <ReminderSlot
        reminder={{ color: 'FAKE_COLOR' }}
        openModal={jest.fn()}
        selectReminder={jest.fn()}
      />
    );
    const button = wrapper.find('button');
    expect(button).toHaveLength(1);
  });
});
