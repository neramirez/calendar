import React from 'react';
import './App.css';
import Calendar from './components/Calendar/Calendar';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';


const App = ({ persistor, store }) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Calendar/>
      </PersistGate>
    </Provider>
  );
};

export default App;
