import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../_reducers';
import apiMiddleware from '../_middleware/axios';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['reminders']
};

// Add support for Redux dev tools
const persistedReducer = persistReducer(persistConfig, rootReducer());
const getStore = () => {
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    return createStore(
      persistedReducer,
      compose(
        applyMiddleware(thunk, apiMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    );
  } else {
    return createStore(
      persistedReducer,
      compose(applyMiddleware(thunk, apiMiddleware))
    );
  }
};
export default function configureStore() {
  const store = getStore();
  if (module.hot) {
    module.hot.accept('../_reducers', () => {
      store.replaceReducer(rootReducer());
    });
  }

  let persistor = persistStore(store);

  return { store, persistor };
}
