import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../_reducers';
import apiMiddleware from '../_middleware/axios';
import thunk from 'redux-thunk';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/es/storage';
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['reminders']
};
// Add support for Redux dev tools
const persistedReducer = persistReducer(persistConfig, rootReducer(history));

export default function configureStore() {
  const store = createStore(
    persistedReducer,
    applyMiddleware(thunk, apiMiddleware)
  );
  if (module.hot) {
    module.hot.accept('../_reducers', () => {
      store.replaceReducer(rootReducer(history));
    });
  }

  let persistor = persistStore(store);

  return { store, persistor };
}
