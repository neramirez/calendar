import { DEFAULT_SUCCESS_SUFFIX, GET_FORECAST } from '../_actions/types';

export default function(state = { byCityName: {} }, action) {
  switch (action.type) {
    case `${GET_FORECAST}${DEFAULT_SUCCESS_SUFFIX}`: {
      const { city, list } = action.payload;
      return {
        ...state,
        byCityName: {
          ...state.byCityName,
          [city.name]: {
            icon: list[0].weather[0].icon
          }
        }
      };
    }
    default:
      return state;
  }
}
