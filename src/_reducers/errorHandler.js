import { API_START, SHOW_ERROR_ACTION } from "../_actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case API_START:
      return [];
    case SHOW_ERROR_ACTION:
      return [action.payload];
    default:
      return state;
  }
}
