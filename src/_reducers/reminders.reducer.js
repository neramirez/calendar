import {
  ADD_REMINDER,
  DELETE_ALL_REMINDERS,
  DELETE_REMINDER
} from '../_actions/types';
import { DATE_FORMAT } from '../_selectors/remindersSelectors';
import moment from 'moment';
import { omit, filter, keyBy } from 'lodash';
const uuidv4 = require('uuid/v4');

export default function(state = { items: {} }, action) {
  switch (action.type) {
    case ADD_REMINDER: {
      const reminder = action.payload;
      const id = reminder.id || uuidv4();
      return {
        ...state,
        items: {
          ...state.items,
          [id]: {
            id: id,
            ...reminder,
            date: moment(reminder.date_time).format(DATE_FORMAT)
          }
        }
      };
    }
    case DELETE_REMINDER: {
      return { ...state, items: omit(state.items, action.payload) };
    }

    case DELETE_ALL_REMINDERS: {
      const selectedDate = action.payload;
      return {
        ...state,
        items: keyBy(
          filter(state.items, ({ date }) => date !== selectedDate),
          'id'
        )
      };
    }

    default:
      return state;
  }
}
