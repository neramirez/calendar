import { combineReducers } from 'redux';
import errorHandler from './errorHandler';
import reminders from './reminders.reducer';
import modal from './modal.reducer';
import api from './api.reducer';
import selectedReminder from './selectedReminder.reducer';
import openWeatherMap from './openweathermap.reducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = () => {
  const appReducer = combineReducers({
    errorHandler,
    reminders,
    selectedReminder,
    modal,
    openWeatherMap,
    api,
    form: formReducer
  });
  return (state, action) => {
    return appReducer(state, action);
  };
};
export default rootReducer;
