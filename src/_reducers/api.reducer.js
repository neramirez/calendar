import { API_START, API_END } from '../_actions/types';

export default function(state = { loading: false }, action) {
  switch (action.type) {
    case API_START: {
      return { ...state, loading: true };
    }
    case API_END: {
      return { ...state, loading: false };
    }
    default:
      return state;
  }
}
