import { CLOSE_MODAL, SELECT_REMINDER } from '../_actions/types';
import { omit } from 'lodash';
export default function(state = { selected: {} }, action) {
  switch (action.type) {
    case SELECT_REMINDER: {
      return { ...state, selected: { id: action.payload } };
    }
    case CLOSE_MODAL: {
      return { ...state, selected: { ...omit(state.selected, 'id') } };
    }
    default:
      return state;
  }
}
