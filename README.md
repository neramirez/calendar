# Front end Calendar Challenge

Thanks for allowing me to take this challenge I have a lot of fun while working on it

## Designs approach

### The calendar

The calendar is implemented with a CSS grid and several flex box children,
I implemented minor responsive stuff's, I think what i have done is enough to display a calendar correctly, however,
I'm aware of UX issues on touch and small displays/devices

### Open Weather Map

I decided to choose the first result of the weather forecast as the forecast data is limited to 15 days, so, by default
I'm displaying the most immediate result.

For this to be achieved project is using Axios

### Storage

Im persisting the reminders state to avoid loosing data between refresh pages

### Connected Components

I'm using single exported connected components, even tho I know this can be controversial I choose to make it this way
due the simplicity of this implementation, a _Containerized_ approach can be done quickly

### Edit a reminder modal

I added react-modal dependency to open a modal that contains the redux-form

### Overflow on calendar slots

I added a simple `overflow-y:scroll` to allow user to see all of their reminders, a more elegan solutions will be to add a
`more ...` button if all items doesnt fit on their parent container

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`
